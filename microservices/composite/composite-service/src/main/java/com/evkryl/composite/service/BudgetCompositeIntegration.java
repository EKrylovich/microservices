package com.evkryl.composite.service;

import com.evkryl.composite.model.external.Budget;
import com.evkryl.composite.model.external.Cost;
import com.evkryl.composite.model.external.Scenario;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestOperations;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * Created by ekrylovich
 * on 12.5.17.
 */
@Component
public class BudgetCompositeIntegration {
	private static final Logger LOG = LoggerFactory.getLogger(BudgetCompositeIntegration.class);

//	@LoadBalanced
	private final RestOperations restTemplate;

//	@Autowired
//	private ServiceUtils utils;

	@Autowired
	public BudgetCompositeIntegration(RestOperations restTemplate) {
		this.restTemplate = restTemplate;
	}

	@HystrixCommand(fallbackMethod = "defaultBudget")
	public ResponseEntity<Budget> getBudget(int budgetId) {

		LOG.debug("Will call getBudget with Hystrix protection");

		String url = "http://budget-service/budget/" + budgetId;
		LOG.debug("GetProduct from URL: {}", url);

		ResponseEntity<String> resultStr = restTemplate.getForEntity(url, String.class);
		LOG.debug("GetBudget http-status: {}", resultStr.getStatusCode());
		LOG.debug("GetBudget body: {}", resultStr.getBody());

		Budget budget = response2Budget(resultStr);
		LOG.debug("GetBudget.id: {}", budget.getBudgetId());

		return new ResponseEntity<>(budget, HttpStatus.OK);
	}

	/**
	 * Fallback method for getBudget()
	 *
	 * @param budgetId
	 * @return
	 */
	public ResponseEntity<Budget> defaultBudget(int budgetId) {
		LOG.warn("Using fallback method for budget-service");
		return new ResponseEntity<>(
				new Budget(budgetId, 0d, "Fallback Name", "Fallback Goal"),
				HttpStatus.OK);
	}

	@HystrixCommand(fallbackMethod = "defaultCosts")
	public ResponseEntity<List<Cost>> getCosts(int budgetId) {
		LOG.debug("Will call getCosts with Hystrix protection");

		String url = "http://cost-service/cost?budgetId=" + budgetId;
		LOG.debug("GetCosts from URL: {}", url);

		ResponseEntity<String> resultStr = restTemplate.getForEntity(url, String.class);
		LOG.debug("GetCosts http-status: {}", resultStr.getStatusCode());
		LOG.debug("GetCosts body: {}", resultStr.getBody());

		List<Cost> costs = response2Costs(resultStr);
		LOG.debug("GetCosts.cnt {}", costs.size());

		return new ResponseEntity<>(costs, HttpStatus.OK);
	}

	/**
	 * Fallback method for getCosts()
	 *
	 * @param budgetId
	 * @return
	 */
	public ResponseEntity<List<Cost>> defaultCosts(int budgetId) {
		LOG.warn("Using fallback method for cost-service");
		return new ResponseEntity<>(
				Collections.singletonList(new Cost(0, budgetId, "Fallback Entity", 0d)),
				HttpStatus.OK);
	}

	@HystrixCommand(fallbackMethod = "defaultScenarios")
	public ResponseEntity<List<Scenario>> getScenarios(int budgetId) {
		LOG.debug("Will call getScenarios with Hystrix protection");

		String url = "http://scenario-service/scenario?budgetId=" + budgetId;
		LOG.debug("GetScenarios from URL: {}", url);

		ResponseEntity<String> resultStr = restTemplate.getForEntity(url, String.class);
		LOG.debug("GetScenarios http-status: {}", resultStr.getStatusCode());
		LOG.debug("GetScenarios body: {}", resultStr.getBody());

		List<Scenario> scenarios = response2Scenario(resultStr);
		LOG.debug("GetScenarios.cnt {}", scenarios.size());


		return new ResponseEntity<>(scenarios, HttpStatus.OK);
	}

	/**
	 * Fallback method for getScenarios()
	 *
	 * @param budgetId
	 * @return
	 */
	public ResponseEntity<List<Scenario>> defaultScenarios(int budgetId) {
		LOG.warn("Using fallback method for scenario-service");
		return new ResponseEntity<>(
				Collections.singletonList(new Scenario(0, budgetId, "Fallback Entity",
						"Fallback Subject", "Fallback Scenraio")),
				HttpStatus.OK);
	}

    /*
	 * TODO: Extract to a common util-lib
     */

	private ObjectReader budgetReader = null;

	private ObjectReader getBudgetReader() {

		if (budgetReader != null) return budgetReader;

		ObjectMapper mapper = new ObjectMapper();
		return budgetReader = mapper.reader(Budget.class);
	}

	private ObjectReader reviewsReader = null;

	private ObjectReader getReviewsReader() {
		if (reviewsReader != null) return reviewsReader;

		ObjectMapper mapper = new ObjectMapper();
		return reviewsReader = mapper.reader(new TypeReference<List<Scenario>>() {
		});
	}

	public Budget response2Budget(ResponseEntity<String> response) {
		try {
			return getBudgetReader().readValue(response.getBody());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	// TODO: Gereralize with <T> method, skip objectReader objects!
	private List<Cost> response2Costs(ResponseEntity<String> response) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			List list = mapper.readValue(response.getBody(), new TypeReference<List<Cost>>() {
			});
			List<Cost> recommendations = list;
			return recommendations;

		} catch (IOException e) {
			LOG.warn("IO-err. Failed to read JSON", e);
			throw new RuntimeException(e);

		} catch (RuntimeException re) {
			LOG.warn("RTE-err. Failed to read JSON", re);
			throw re;
		}
	}

	private List<Scenario> response2Scenario(ResponseEntity<String> response) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			List list = mapper.readValue(response.getBody(), new TypeReference<List<Scenario>>() {
			});
			List<Scenario> reviews = list;
			return reviews;

		} catch (IOException e) {
			LOG.warn("IO-err. Failed to read JSON", e);
			throw new RuntimeException(e);

		} catch (RuntimeException re) {
			LOG.warn("RTE-err. Failed to read JSON", re);
			throw re;
		}
	}
}
