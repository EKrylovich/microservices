package com.evkryl.composite.service;

import com.evkryl.composite.model.BudgetAggregated;
import com.evkryl.composite.model.external.Budget;
import com.evkryl.composite.model.external.Cost;
import com.evkryl.composite.model.external.Scenario;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import java.util.*;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * Created by ekrylovich
 * on 12.5.17.
 */
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@RestController
public class BudgetCompositeService {

	private static final Logger LOG = LoggerFactory.getLogger(BudgetCompositeService.class);

	private final BudgetCompositeIntegration integration;

	@Autowired
	public BudgetCompositeService(BudgetCompositeIntegration integration) {
		this.integration = integration;
	}

	@RequestMapping("/budget/{budgetId}")
	public ResponseEntity<BudgetAggregated> getBudget(@PathVariable int budgetId) {

		LOG.info("Synch start...");
		Budget budget = getBasicBudgetInfo(budgetId);
		List<Cost> costs = getBasicCostsInfo(budgetId);
		List<Scenario> scenarios = getBasicScenariosInfo(budgetId);

		return new ResponseEntity<>(new BudgetAggregated(budget, costs, scenarios), HttpStatus.OK);
	}

	private Budget getBasicBudgetInfo(int budgetId) {
		ResponseEntity<Budget> budgetResult = integration.getBudget(budgetId);
		Optional<Budget> costs = processResponse(budgetResult);
		return costs.orElse(new Budget(0, 0,"Empty", "Empty"));
	}

	private List<Cost> getBasicCostsInfo(int budgetId) {
		ResponseEntity<List<Cost>> costsResult = integration.getCosts(budgetId);
		Optional<List<Cost>> costs = processResponse(costsResult);
		return costs.orElse(Collections.emptyList());
	}

	private List<Scenario> getBasicScenariosInfo(int budgetId) {
		ResponseEntity<List<Scenario>> scenarioResult = integration.getScenarios(budgetId);
		Optional<List<Scenario>> scenarios = processResponse(scenarioResult);
		return scenarios.orElse(Collections.emptyList());
	}

	private <T> Optional<T> processResponse(final ResponseEntity<T> responseEntity){
		Optional<T> result = Optional.empty();
		if (!responseEntity.getStatusCode().is2xxSuccessful()) {
			LOG.debug("Call failed: {}", responseEntity.getStatusCode());
		} else {
			result = Optional.of(responseEntity.getBody());
		}
		return result;
	}
}
