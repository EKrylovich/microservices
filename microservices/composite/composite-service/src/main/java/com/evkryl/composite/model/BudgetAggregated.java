package com.evkryl.composite.model;

import com.evkryl.composite.model.external.Budget;
import com.evkryl.composite.model.external.Cost;
import com.evkryl.composite.model.external.Scenario;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ekrylovich
 * on 12.5.17.
 */
public class BudgetAggregated {
	private int budgetId;
	private double amount;
	private String owner;
	private String goal;
	private List<CostSummary> costs;
	private List<ScenarioSummary> scenarios;

	public BudgetAggregated(Budget budget, List<Cost> costs, List<Scenario> scenarios) {

		// 1. Setup budget info
		this.budgetId = budget.getBudgetId();
		this.amount = budget.getAmount();
		this.owner =  budget.getOwner();
		this.goal =  budget.getGoal();

		// 2. Copy summary recommendation info, if available
		if (costs != null)
			this.costs = costs.stream()
					.map(r -> new CostSummary(r.getCostId(), r.getPrice(), r.getEntity()))
					.collect(Collectors.toList());

		// 3. Copy summary review info, if available
		if (scenarios != null)
			this.scenarios = scenarios.stream()
					.map(r -> new ScenarioSummary(r.getScenarioId(), r.getAuthor(), r.getScenario()))
					.collect(Collectors.toList());

	}

	public int getBudgetId() {
		return budgetId;
	}

	public double getAmount() {
		return amount;
	}

	public String getOwner() {
		return owner;
	}

	public String getGoal() {
		return goal;
	}

	public List<CostSummary> getCosts() {
		return costs;
	}

	public List<ScenarioSummary> getScenarios() {
		return scenarios;
	}
}
