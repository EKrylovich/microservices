package com.evkryl.budget.service;

import com.evkryl.budget.model.Budget;
import com.evkryl.budget.service.util.SetProcTimeBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * Created by ekrylovich
 * on 12.5.17.
 */
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@RestController
public class BudgetService {
	private static final Logger LOG = LoggerFactory.getLogger(BudgetService.class);

	private final SetProcTimeBean setProcTimeBean;

	@Autowired
	public BudgetService(final SetProcTimeBean setProcTimeBean) {
		this.setProcTimeBean = setProcTimeBean;
	}

	/**
	 * Sample usage: curl $HOST:$PORT/budget/1
	 *
	 * @param budgetId
	 * @return
	 */
	@RequestMapping("/budget/{budgetId}")
	public Budget getBudget(@PathVariable int budgetId) {

		int pt = setProcTimeBean.calculateProcessingTime();
		LOG.info("/budget called, processing time: {}", pt);

		sleep(pt);

		LOG.debug("/budget return the found budget");
		return new Budget(budgetId, 123.0, "Egor Krylovich", "New modern application");
	}


	/**
	 * Sample usage:
	 *
	 *  curl "http://localhost:10002/set-processing-time?minMs=1000&maxMs=2000"
	 *
	 * @param minMs
	 * @param maxMs
	 */
	@RequestMapping("/set-processing-time")
	public void setProcessingTime(
			@RequestParam(value = "minMs", required = true) int minMs,
			@RequestParam(value = "maxMs", required = true) int maxMs) {

		LOG.info("/set-processing-time called: {} - {} ms", minMs, maxMs);

		setProcTimeBean.setDefaultProcessingTime(minMs, maxMs);
	}

	private void sleep(int pt) {
		try {
			Thread.sleep(pt);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
