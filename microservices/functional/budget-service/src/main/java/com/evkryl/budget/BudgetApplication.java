package com.evkryl.budget;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * Created by ekrylovich
 * on 6.5.17.
 */
@EnableDiscoveryClient
@SpringBootApplication
public class BudgetApplication {

	private static final Logger LOG = LoggerFactory.getLogger(BudgetApplication.class);

	public static void main(String[] args) {

		ConfigurableApplicationContext ctx = SpringApplication.run(BudgetApplication.class, args);

		LOG.info("Connected to RabbitMQ at: {}", ctx.getEnvironment().getProperty("spring.rabbitmq.host"));
	}
}
