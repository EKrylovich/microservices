package com.evkryl.cost;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * Created by ekrylovich
 * on 6.5.17.
 */
@EnableDiscoveryClient
@SpringBootApplication
public class CostApplication {
	public static void main(String[] args) {
		SpringApplication.run(CostApplication.class, args);
	}
}
