package com.evkryl.cost.service;


import com.evkryl.cost.model.Cost;
import com.evkryl.cost.service.util.SetProcTimeBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import java.util.ArrayList;
import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * Created by ekrylovich
 * on 12.5.17.
 */
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@RestController
public class CostService {
	private static final Logger LOG = LoggerFactory.getLogger(CostService.class);

	private final SetProcTimeBean setProcTimeBean;

	@Autowired
	public CostService(final SetProcTimeBean setProcTimeBean) {
		this.setProcTimeBean = setProcTimeBean;
	}


    /**
	 * Sample usage: curl $HOST:$PORT/cost?budgetId=1
	 *
	 * @param budgetId
	 * @return
	 */
	@RequestMapping("/cost")
	public List<Cost> getCosts(@RequestParam(value = "budgetId") int budgetId) {

		int pt = setProcTimeBean.calculateProcessingTime();
		LOG.info("/reviews called, processing time: {}", pt);

		sleep(pt);

		List<Cost> list = new ArrayList<>();
		list.add(new Cost(1, budgetId, "VM",1_000));
		list.add(new Cost(2, budgetId, "Repository",500));
		list.add(new Cost(3, budgetId, "Engineer's salary",100_000));

		LOG.info("/reviews response size: {}", list.size());

		return list;
	}

	private void sleep(int pt) {
		try {
			Thread.sleep(pt);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sample usage:
	 *
	 *  curl "http://localhost:10002/set-processing-time?minMs=1000&maxMs=2000"
	 *
	 * @param minMs
	 * @param maxMs
	 */
	@RequestMapping("/set-processing-time")
	public void setProcessingTime(
			@RequestParam(value = "minMs") int minMs,
			@RequestParam(value = "maxMs") int maxMs) {

		LOG.info("/set-processing-time called: {} - {} ms", minMs, maxMs);

		setProcTimeBean.setDefaultProcessingTime(minMs, maxMs);
	}
}
