package com.evkryl.api.budget;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.client.RestTemplate;

/**
 * Created by ekrylovich
 * on 17.5.17.
 */
@SpringBootApplication
@EnableCircuitBreaker
@EnableResourceServer
@EnableDiscoveryClient
public class BudgetApiServiceApplication{
	public static void main(String[] args) {
		SpringApplication.run(BudgetApiServiceApplication.class, args);
	}

	@LoadBalanced
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
}
