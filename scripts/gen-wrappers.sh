#!/usr/bin/env bash

function note() {
    local GREEN NC
    GREEN='\033[0;32m'
    NC='\033[0m' # No Color
    printf "\n${GREEN}$@  ${NC}\n" >&2
}
set -e

cd ../microservices/functional/budget-service;           note "Wrapper creation budget...";          ./gradlew wrapper; cd -
cd ../microservices/functional/cost-service;             note "Wrapper creation cost...";            ./gradlew wrapper; cd -
cd ../microservices/functional/scenario-service;         note "Wrapper creation scenario...";        ./gradlew wrapper; cd -
cd ../microservices/composite/composite-service;         note "Wrapper creation composite...";       ./gradlew wrapper; cd -
cd ../microservices/api/budget-api-service;              note "Wrapper creation api...";             ./gradlew wrapper; cd -

cd ../microservices/support/auth-server;                 note "Wrapper creation auth...";            ./gradlew wrapper; cd -
cd ../microservices/support/discovery-server;            note "Wrapper creation discovery...";       ./gradlew wrapper; cd -
cd ../microservices/support/edge-server;                 note "Wrapper creation edge...";            ./gradlew wrapper; cd -
cd ../microservices/support/monitor-dashboard;           note "Wrapper creation monitor...";         ./gradlew wrapper; cd -
cd ../microservices/support/turbine;                     note "Wrapper creation turbine...";         ./gradlew wrapper; cd -